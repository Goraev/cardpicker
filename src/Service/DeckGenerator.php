<?php

namespace App\Service;

use App\DomainObject\Card;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

class DeckGenerator
{
    /**
     * @var ContainerBagInterface
     */
    private $params;

    /**
     * @var array
     */
    private $deck ;

    public function __construct(ContainerBagInterface $params)
    {
        $this->params = $params;
        $this->create();
    }

    private function create()
    {
        foreach ($this->params->get('app.card.colors') as $colorIndex => $color) {
            foreach ($this->params->get('app.card.ranks') as $rankIndex => $rank) {
                $this->deck[] = new Card($colorIndex, $rankIndex);
            }
        }
    }

    /**
     * @return DeckGenerator
     */
    public function shuffle(): self
    {
        shuffle($this->deck);

        return $this;
    }

    public function getDeck()
    {
        return $this->deck;
    }
}
