<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

abstract class RandomOrder
{
    /**
     * @var string
     */
    protected $parameterName;

    /**
     * RandomOrder constructor.
     * @param ContainerBagInterface $params
     */
    public function __construct(ContainerBagInterface $params)
    {
        $this->params = $params;
    }

    /**
     * @return array
     */
    public function get(): array
    {
        $randomOrder = array_keys($this->params->get($this->parameterName));
        shuffle($randomOrder);

        return $randomOrder;
    }
}
