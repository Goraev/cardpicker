<?php

namespace App\DomainObject;

class Hand
{
    /**
     * @var Card[]
     */
    private $deck;

    /**
     * @var Card[]
     */
    private $cards;

    /**
     * @var Card[]
     */
    private $sortedCards;

    /**
     * @var array
     */
    private $colorOrder;


    /**
     * @var array
     */
    private $rankOrder;

    /**
     * @param Card[] $deck
     * @param array $colorOrder
     * @param array $rankOrder
     */
    public function __construct(array $deck, array $colorOrder, array $rankOrder)
    {
        $this->deck = $deck;
        $this->colorOrder = $colorOrder;
        $this->rankOrder = $rankOrder;
    }

    /**
     * @param Card[] $cards
     */
    public function setCards(array $cards)
    {
        $this->cards = $cards;
    }


    /**
     * @param Card $card
     */
    public function addCard(Card $card)
    {
        $this->cards[] = $card;
    }

    /**
     * @param array $colorOrder
     */
    public function setColorOrder(array $colorOrder)
    {
        $this->colorOrder = $colorOrder;
    }

    /**
     * @param array $rankOrder
     */
    public function setRankOrder(array $rankOrder)
    {
        $this->rankOrder = $rankOrder;
    }

    /**
     * @param int $number
     */
    public function pickCardsInDeck(int $number)
    {
        $cardNumberInDeck = count($this->deck);

        $pickedIndexes = [];

        while (count($pickedIndexes) < $number) {
            $randomIndex = rand(0, $cardNumberInDeck-1);

            if (!in_array($randomIndex, $pickedIndexes)) {
                $this->addCard($this->deck[$randomIndex]);
                $pickedIndexes[] = $randomIndex;
            }
        }
    }

    public function sortCards()
    {
        if (1 >= count($this->cards)) {
            return 0;
        }

        $this->sortedCards = $this->cards;

        $colorOrder = $this->colorOrder;
        $rankOrder = $this->rankOrder;

        usort($this->sortedCards, function($a, $b) use ($rankOrder, $colorOrder) {
            if (array_search($a->getColor(), $colorOrder) === array_search($b->getColor(), $colorOrder)) {
                return array_search($a->getRank(), $rankOrder) > array_search($b->getRank(), $rankOrder) ? 1 : -1;
            }

            return array_search($a->getColor(), $colorOrder) > array_search($b->getColor(), $colorOrder) ? 1 : -1;
        });
    }

    /**
     * @return Card[]
     */
    public function getCards(): array
    {
        return $this->cards;
    }

    /**
     * @return Card[]
     */
    public function getSortedCards(): array
    {
        return $this->sortedCards;
    }
}
