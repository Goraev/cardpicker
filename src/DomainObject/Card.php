<?php

namespace App\DomainObject;

class Card
{
    /**
     * @var int
     */
    private $color;

    /**
     * @var int
     */
    private $rank;

    /**
     * Card constructor.
     * @param int $color
     * @param int $rank
     */
    public function __construct(int $color, int $rank)
    {
        $this->color = $color;
        $this->rank = $rank;
    }

    /**
     * @return int
     */
    public function getColor(): int
    {
        return $this->color;
    }

    /**
     * @param int $color
     */
    public function setColor(int $color)
    {
        $this->color = $color;
    }

    /**
     * @return int
     */
    public function getRank(): int
    {
        return $this->rank;
    }

    /**
     * @param int $rank
     */
    public function setRank(int $rank)
    {
        $this->rank = $rank;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'color' => $this->getColor(),
            'rank' => $this->getRank(),
        ];
    }
}
