<?php

namespace App\Helper;

use App\DomainObject\Card;

class CardCollectionToArrayHelper
{
    /**
     * @param array $collectionCard
     * @return array
     */
    public function transform(array $collectionCard): array
    {
        $arrayCard = [];

        foreach($collectionCard as $position => $card) {
            $arrayCard[] = [
                'position' => $position,
                'color' => $card->getColor(),
                'rank' => $card->getRank(),
            ];
        }

        return $arrayCard;
    }

    /**
     * @param array $arrayCard
     * @return array
     */
    public function revert(array $arrayCard): array
    {
        $collectionCard = [];

        foreach ($arrayCard as $card) {
            $collectionCard[] = new Card($card['color'], $card['rank']);
        }

        return $collectionCard;
    }
}