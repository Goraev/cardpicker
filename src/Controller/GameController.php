<?php

namespace App\Controller;

use App\DomainObject\Hand;
use App\Helper\CardCollectionToArrayHelper;
use App\Service\ColorRandomOrder;
use App\Service\DeckGenerator;
use App\Service\RankRandomOrder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GameController extends AbstractController
{
    /**
     * @param DeckGenerator $deckGenerator
     * @param CardCollectionToArrayHelper $converter
     *
     * @return Response
     */
    public function index(DeckGenerator $deckGenerator, CardCollectionToArrayHelper $converter): Response
    {
        $deck = $deckGenerator->getDeck();

        return $this->render('game.html.twig', [
            'deck' => json_encode($converter->transform($deck)),
        ]);
    }

    /**
     * @param Request $request
     * @param DeckGenerator $deckGenerator
     * @param CardCollectionToArrayHelper $converter
     *
     * @return JsonResponse|Response
     */
    public function shuffleDeck(Request $request, DeckGenerator $deckGenerator, CardCollectionToArrayHelper $converter)
    {
        if (!$request->isXmlHttpRequest()) {
            return new Response('Forbidden', 403);
        }

        return $this->json(
            $converter->transform(
                $deckGenerator
                    ->shuffle()
                    ->getDeck()
            )
        );
    }

    /**
     * @param Request $request
     * @param CardCollectionToArrayHelper $converter
     *
     * @return JsonResponse|Response
     */
    public function pickInDeck(Request $request, CardCollectionToArrayHelper $converter)
    {
        if (!$request->isXmlHttpRequest()) {
            return new Response('Forbidden', 403);
        }

        $content = json_decode($request->getContent(), true);


        $hand = new Hand($converter->revert($content['deck']), [], []);


        $hand->pickCardsInDeck($content['number']);

        $session = $request->getSession();
        $session->set('hand', $hand);

        return $this->json($converter->transform($hand->getCards()));
    }

    /**
     * @param Request $request
     * @param ColorRandomOrder $colorRandomOrder
     *
     * @return JsonResponse|Response
     */
    public function shuffleColorOrder(Request $request, ColorRandomOrder $colorRandomOrder)
    {
        if (!$request->isXmlHttpRequest()) {
            return new Response('Forbidden', 403);
        }

        return $this->json(
            $colorRandomOrder->get()
        );
    }

    /**
     * @param Request $request
     * @param RankRandomOrder $rankRandomOrder
     *
     * @return JsonResponse|Response
     */
    public function shuffleRankOrder(Request $request, RankRandomOrder $rankRandomOrder)
    {
        if (!$request->isXmlHttpRequest()) {
            return new Response('Forbidden', 403);
        }

        return $this->json(
            $rankRandomOrder->get()
        );
    }

    /**
     * @param Request $request
     * @param CardCollectionToArrayHelper $converter
     *
     * @return JsonResponse|Response
     */
    public function orderHand(Request $request, CardCollectionToArrayHelper $converter)
    {
        if (!$request->isXmlHttpRequest()) {
            return new Response('Forbidden', 403);
        }

        $content = json_decode($request->getContent(), true);

        $session = $request->getSession();
        $hand = $session->get('hand');
        $hand->setColorOrder($content['colorOrder']);
        $hand->setRankOrder($content['rankOrder']);
        $hand->sortCards();
        $session->set('hand', $hand);

        return $this->json($converter->transform($hand->getSortedCards()));
    }
}
