<?php

namespace App\Tests\DomainObject;

use App\DomainObject\Card;
use PHPUnit\Framework\TestCase;

class CardTest extends TestCase
{
    public function testToArray()
    {
        $card = new Card(99,132);
        $this->assertEquals($card->toArray(), ['color' => 99, 'rank' => 132]);
    }
}