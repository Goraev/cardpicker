<?php

namespace App\Tests\DomainObject;

use App\DomainObject\Card;
use App\DomainObject\Hand;
use PHPUnit\Framework\TestCase;

class HandTest extends TestCase
{
    public function testPickCardsInDeck()
    {
        $cardOne = new Card(1,111);
        $cardTwo = new Card(23,34);
        $cardThree = new Card(8,328);
        $cardFour = new Card(4,6);

        $hand = new Hand([$cardOne, $cardTwo, $cardThree, $cardFour], [31, 328, 34, 4], [8, 1, 23, 6]);
        $hand->pickCardsInDeck(2);

        $this->assertEquals(count($hand->getCards()), 2);
    }

    public function testSortCards()
    {
        $cardOne = new Card(0,1);
        $cardTwo = new Card(0,2);
        $cardThree = new Card(1,3);
        $cardFour = new Card(2,6);
        $cardFive = new Card(2,4);
        $cardSix = new Card(2,5);

        $hand = new Hand(
            [$cardOne, $cardTwo, $cardThree, $cardFour, $cardFive, $cardSix],
            [1, 0, 2],
            [4, 1, 6, 3, 5, 2]
        );
        $hand->setCards([$cardOne, $cardTwo, $cardThree, $cardFour, $cardFive, $cardSix]);
        $hand->sortCards();

        $expectedSortedCards = [
            //1
            $cardThree,

            //0
            $cardOne,
            $cardTwo,

            //2
            $cardFive,
            $cardFour,
            $cardSix,
        ];

        $this->assertEquals($expectedSortedCards, $hand->getSortedCards());
    }
}
