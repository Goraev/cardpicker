<?php

namespace App\Tests\Helper;

use App\DomainObject\Card;
use App\Helper\CardCollectionToArrayHelper;
use PHPUnit\Framework\TestCase;

class CardCollectionToArrayHelperTest extends TestCase
{
    public function testTransform()
    {
        $helper = new CardCollectionToArrayHelper();

        $cardOne = new Card(0,1);
        $cardTwo = new Card(0,2);
        $cardThree = new Card(1,3);

        $deck = [$cardOne, $cardTwo, $cardThree];
        ;

        $this->assertEquals(
            [
                [
                    'position' => 0,
                    'color' => 0,
                    'rank' => 1
                ],
                [
                    'position' => 1,
                    'color' => 0,
                    'rank' => 2
                ],
                [
                    'position' => 2,
                    'color' => 1,
                    'rank' => 3
                ],

            ],
            $helper->transform($deck)
        );
    }

    public function testRevert()
    {
        $helper = new CardCollectionToArrayHelper();

        $array = [
            [
                'position' => 0,
                'color' => 0,
                'rank' => 1
            ],
            [
                'position' => 1,
                'color' => 0,
                'rank' => 2
            ],
            [
                'position' => 2,
                'color' => 1,
                'rank' => 3
            ],

        ];

        $this->assertEquals(
            [
                new Card(0,1),
                new Card(0,2),
                new Card(1,3),
            ],
            $helper->revert($array)
        );
    }
}